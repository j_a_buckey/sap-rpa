enableTypeOnScreen();


        switchToExistingWindow(new com.workfusion.studio.rpa.recorder.api.groovy.WindowDescriptor("SAP_FRONTEND_SESSION", "SAP Easy Access", false, false).toString(), 10000);


                mouse().click($(byImage("1518015389567-anchor.apng", Integer.valueOf(0), Integer.valueOf(0))).getCoordinates());
    sleep(3000);



            def i0 = tcode;

			if (tcode instanceof com.workfusion.studio.rpa.recorder.api.groovy.RecorderList) {
				i0 = new com.workfusion.studio.rpa.recorder.api.groovy.FilterExpression("").applyTo(tcode);

			} else if (tcode instanceof com.workfusion.studio.rpa.recorder.api.groovy.RecorderTable) {
				i0 = new com.workfusion.studio.rpa.recorder.api.groovy.FilterExpression("").applyTo(tcode);

			} else if (tcode instanceof BigDecimal) {
				i0 = tcode.toPlainString();
			}

            sendKeys(com.workfusion.studio.rpa.recorder.api.groovy.StringTransformations.escapeAutoitText(String.valueOf(i0)));
    sleep(4900);


                mouse().click($(byImage("1518015401574-anchor.apng", Integer.valueOf(0), Integer.valueOf(0))).getCoordinates());

    sleep(2700);


        switchToExistingWindow(new com.workfusion.studio.rpa.recorder.api.groovy.WindowDescriptor("SAP_FRONTEND_SESSION", "Create Purchase Order", false, false).toString(), 10000);


                mouse().click($(byImage("1518015404438-anchor.apng", Integer.valueOf(0), Integer.valueOf(0))).getCoordinates());
    sleep(2200);



            def i1 = vendor_number;

			if (vendor_number instanceof com.workfusion.studio.rpa.recorder.api.groovy.RecorderList) {
				i1 = new com.workfusion.studio.rpa.recorder.api.groovy.FilterExpression("").applyTo(vendor_number);

			} else if (vendor_number instanceof com.workfusion.studio.rpa.recorder.api.groovy.RecorderTable) {
				i1 = new com.workfusion.studio.rpa.recorder.api.groovy.FilterExpression("").applyTo(vendor_number);

			} else if (vendor_number instanceof BigDecimal) {
				i1 = vendor_number.toPlainString();
			}

            sendKeys(com.workfusion.studio.rpa.recorder.api.groovy.StringTransformations.escapeAutoitText(String.valueOf(i1)));
    sleep(2900);


                mouse().click($(byImage("1518015413558-anchor.apng", Integer.valueOf(0), Integer.valueOf(0))).getCoordinates());



            def i2 = company_code;

			if (company_code instanceof com.workfusion.studio.rpa.recorder.api.groovy.RecorderList) {
				i2 = new com.workfusion.studio.rpa.recorder.api.groovy.FilterExpression("").applyTo(company_code);

			} else if (company_code instanceof com.workfusion.studio.rpa.recorder.api.groovy.RecorderTable) {
				i2 = new com.workfusion.studio.rpa.recorder.api.groovy.FilterExpression("").applyTo(company_code);

			} else if (company_code instanceof BigDecimal) {
				i2 = company_code.toPlainString();
			}

            sendKeys(com.workfusion.studio.rpa.recorder.api.groovy.StringTransformations.escapeAutoitText(String.valueOf(i2)));

    sleep(1500);


        switchToExistingWindow(new com.workfusion.studio.rpa.recorder.api.groovy.WindowDescriptor("SAP_FRONTEND_SESSION", "Create Purchase Order", false, false).toString(), 10000);



        sendKeys(com.workfusion.studio.rpa.recorder.api.groovy.StringTransformations.getKeyPressText(15, 9, 9, 0));



            def i3 = product_group;

			if (product_group instanceof com.workfusion.studio.rpa.recorder.api.groovy.RecorderList) {
				i3 = new com.workfusion.studio.rpa.recorder.api.groovy.FilterExpression("").applyTo(product_group);

			} else if (product_group instanceof com.workfusion.studio.rpa.recorder.api.groovy.RecorderTable) {
				i3 = new com.workfusion.studio.rpa.recorder.api.groovy.FilterExpression("").applyTo(product_group);

			} else if (product_group instanceof BigDecimal) {
				i3 = product_group.toPlainString();
			}

            sendKeys(com.workfusion.studio.rpa.recorder.api.groovy.StringTransformations.escapeAutoitText(String.valueOf(i3)));

    sleep(1400);


        switchToExistingWindow(new com.workfusion.studio.rpa.recorder.api.groovy.WindowDescriptor("SAP_FRONTEND_SESSION", "Create Purchase Order", false, false).toString(), 10000);



        sendKeys(com.workfusion.studio.rpa.recorder.api.groovy.StringTransformations.getKeyPressText(15, 9, 9, 0));
    sleep(900);



            def i4 = company_code;

			if (company_code instanceof com.workfusion.studio.rpa.recorder.api.groovy.RecorderList) {
				i4 = new com.workfusion.studio.rpa.recorder.api.groovy.FilterExpression("").applyTo(company_code);

			} else if (company_code instanceof com.workfusion.studio.rpa.recorder.api.groovy.RecorderTable) {
				i4 = new com.workfusion.studio.rpa.recorder.api.groovy.FilterExpression("").applyTo(company_code);

			} else if (company_code instanceof BigDecimal) {
				i4 = company_code.toPlainString();
			}

            sendKeys(com.workfusion.studio.rpa.recorder.api.groovy.StringTransformations.escapeAutoitText(String.valueOf(i4)));

    sleep(3100);


        switchToExistingWindow(new com.workfusion.studio.rpa.recorder.api.groovy.WindowDescriptor("SAP_FRONTEND_SESSION", "Create Purchase Order", false, false).toString(), 10000);


                mouse().click($(byImage("1518015430021-anchor.apng", Integer.valueOf(12), Integer.valueOf(0))).getCoordinates());
    sleep(800);



        sendKeys(com.workfusion.studio.rpa.recorder.api.groovy.StringTransformations.escapeAutoitText("10"));
    sleep(1500);


                mouse().click($(byImage("1518015432541-anchor.apng", Integer.valueOf(0), Integer.valueOf(0))).getCoordinates());
    sleep(900);



            def i5 = tradegood;

			if (tradegood instanceof com.workfusion.studio.rpa.recorder.api.groovy.RecorderList) {
				i5 = new com.workfusion.studio.rpa.recorder.api.groovy.FilterExpression("").applyTo(tradegood);

			} else if (tradegood instanceof com.workfusion.studio.rpa.recorder.api.groovy.RecorderTable) {
				i5 = new com.workfusion.studio.rpa.recorder.api.groovy.FilterExpression("").applyTo(tradegood);

			} else if (tradegood instanceof BigDecimal) {
				i5 = tradegood.toPlainString();
			}

            sendKeys(com.workfusion.studio.rpa.recorder.api.groovy.StringTransformations.escapeAutoitText(String.valueOf(i5)));

    sleep(2900);


        switchToExistingWindow(new com.workfusion.studio.rpa.recorder.api.groovy.WindowDescriptor("SAP_FRONTEND_SESSION", "Create Purchase Order", false, false).toString(), 10000);



        sendKeys(com.workfusion.studio.rpa.recorder.api.groovy.StringTransformations.getKeyPressText(15, 9, 9, 0));



        sendKeys(com.workfusion.studio.rpa.recorder.api.groovy.StringTransformations.getKeyPressText(15, 9, 9, 0));
    sleep(800);



            def i6 = order_quantity;

			if (order_quantity instanceof com.workfusion.studio.rpa.recorder.api.groovy.RecorderList) {
				i6 = new com.workfusion.studio.rpa.recorder.api.groovy.FilterExpression("").applyTo(order_quantity);

			} else if (order_quantity instanceof com.workfusion.studio.rpa.recorder.api.groovy.RecorderTable) {
				i6 = new com.workfusion.studio.rpa.recorder.api.groovy.FilterExpression("").applyTo(order_quantity);

			} else if (order_quantity instanceof BigDecimal) {
				i6 = order_quantity.toPlainString();
			}

            sendKeys(com.workfusion.studio.rpa.recorder.api.groovy.StringTransformations.escapeAutoitText(String.valueOf(i6)));

    sleep(3700);


        switchToExistingWindow(new com.workfusion.studio.rpa.recorder.api.groovy.WindowDescriptor("SAP_FRONTEND_SESSION", "Create Purchase Order", false, false).toString(), 10000);
    sleep(3300);


                mouse().click($(byImage("1518015450964-anchor.apng", Integer.valueOf(0), Integer.valueOf(0))).getCoordinates());
    sleep(1300);



            def i7 = company_code;

			if (company_code instanceof com.workfusion.studio.rpa.recorder.api.groovy.RecorderList) {
				i7 = new com.workfusion.studio.rpa.recorder.api.groovy.FilterExpression("").applyTo(company_code);

			} else if (company_code instanceof com.workfusion.studio.rpa.recorder.api.groovy.RecorderTable) {
				i7 = new com.workfusion.studio.rpa.recorder.api.groovy.FilterExpression("").applyTo(company_code);

			} else if (company_code instanceof BigDecimal) {
				i7 = company_code.toPlainString();
			}

            sendKeys(com.workfusion.studio.rpa.recorder.api.groovy.StringTransformations.escapeAutoitText(String.valueOf(i7)));
    sleep(1800);


                mouse().click($(byImage("1518015455883-anchor.apng", Integer.valueOf(0), Integer.valueOf(0))).getCoordinates());
    sleep(700);



            def i8 = storage_location;

			if (storage_location instanceof com.workfusion.studio.rpa.recorder.api.groovy.RecorderList) {
				i8 = new com.workfusion.studio.rpa.recorder.api.groovy.FilterExpression("").applyTo(storage_location);

			} else if (storage_location instanceof com.workfusion.studio.rpa.recorder.api.groovy.RecorderTable) {
				i8 = new com.workfusion.studio.rpa.recorder.api.groovy.FilterExpression("").applyTo(storage_location);

			} else if (storage_location instanceof BigDecimal) {
				i8 = storage_location.toPlainString();
			}

            sendKeys(com.workfusion.studio.rpa.recorder.api.groovy.StringTransformations.escapeAutoitText(String.valueOf(i8)));
    sleep(2600);


                mouse().click($(byImage("1518015464475-anchor.apng", Integer.valueOf(0), Integer.valueOf(0))).getCoordinates());


                mouse().click($(byImage("1518103895394-anchor-1518103895420.apng", Integer.valueOf(0), Integer.valueOf(0))).getCoordinates());



        switchToExistingWindow(new com.workfusion.studio.rpa.recorder.api.groovy.WindowDescriptor("DialogBox Container Class", "Performance Assistant", false, false).toString(), 10000);

        mouse().mouseDown($(byImage("1518104205027-anchor.apng", Integer.valueOf(-6), Integer.valueOf(0))).getCoordinates());

        mouse().mouseUp($(byImage("1518104206939-anchor.apng", Integer.valueOf(-10), Integer.valueOf(0))).getCoordinates());




        sendKeys(com.workfusion.studio.rpa.recorder.api.groovy.StringTransformations.getHotKeyText(99, 2));




        sendKeys(com.workfusion.studio.rpa.recorder.api.groovy.StringTransformations.getHotKeyText(114, 4));




        sendKeys(com.workfusion.studio.rpa.recorder.api.groovy.StringTransformations.escapeAutoitText("notepad"));




        sendKeys(com.workfusion.studio.rpa.recorder.api.groovy.StringTransformations.getKeyPressText(28, 13, 10, 0));




        sendKeys(com.workfusion.studio.rpa.recorder.api.groovy.StringTransformations.getHotKeyText(118, 2));

